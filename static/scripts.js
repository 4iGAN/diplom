// var u2fApi = require( 'u2f-api' );

console.log("HelloWorld JS");





function login_json(){ 
    var data = {
        'username': $('#login_username').val(),
        'password': $('#login_password').val()
    };
    console.log(data);
    $.ajax({
        url: '/login/json',
        contentType:"application/json; charset=utf-8",
        method: 'post',
        data: JSON.stringify(data),
        success:function(data) {
            if (data['status']){
                window.location.reload();
            }else{
                $('#login_password').addClass('is-danger');
            }
        }
    });
}

$( document ).ready(function() {
    $( "#login_button" ).click(login_json);
});