'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

// sass
gulp.task('sass', function () {
  return gulp.src('./static/style.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./static/'));
});

// JS
gulp.task('scripts', function() {
  return gulp.src('./static/scripts.js')
    .pipe(concat('js.js'))
    .pipe(gulp.dest('./static/'));
});
 
gulp.task('default', function () {
  gulp.watch('./static/style.sass', ['sass']);
  gulp.watch('./static/scripts.js', ['scripts']);

});