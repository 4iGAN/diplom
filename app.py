from flask import Flask, session, render_template,\
    url_for, request, redirect, jsonify
from flask_fido_u2f import U2F


DB = {
    'ch': '1234',
    'admin': 'qwer',
}

app = Flask(__name__)
u2f = U2F(app)
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'


app.config['U2F_APPID'] = 'http://localhost:5000'
app.config['U2F_FACETS_ENABLED'] = False
app.config['U2F_FACETS_LIST'] = [
    'http://localhost:5000',
]


@app.route("/")
def index():
    if 'is_login' in session:
        return render_template('index.html')
    else:
        return redirect(url_for('login'))


@app.route('/login', methods=['GET'])
def login():
    if 'is_login' in session:
        return redirect(url_for('index'))
    else:
        u2f.enable_sign()
        u2f.enable_enroll()
        u2f.enable_device_management()
        return render_template('login.html')


@app.route('/login/json', methods=['POST'])
def login_json():
    print(request.get_json(silent=True))

    form_username = request.get_json().get('username', None)
    form_password = request.get_json().get('password', None)
    result = {'status': False}
    if form_username in DB.keys():
        if DB[form_username] == form_password:
            session['is_login'] = form_username
            result['status'] = True
    return jsonify(result)


@app.route('/logout')
def logout():
    session.pop('is_login', None)
    return redirect(url_for('index'))


# U2F #####################################################

u2f_data = []


@u2f.read
def read():
    return u2f_data

@u2f.save
def save(u2fdata):
    u2f_data = u2fdata

@u2f.enroll_on_success
def enroll_on_success():
    print('Successfully enrolled new device!')

@u2f.enroll_on_fail
def enroll_on_fail():
    print('Failed to enroll new device!')

@u2f.sign_on_success
def sign_on_success():
    print('Successfully verified user!')

@u2f.sign_on_fail
def sign_on_fail():
    print('Failed to verified user!')



if __name__ == "__main__":
    app.run()
